package teme.proiect.Budget_Tracker.dto;

public enum Type {
    INCOME,
    EXPENSE
}
