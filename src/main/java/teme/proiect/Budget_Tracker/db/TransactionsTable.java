package teme.proiect.Budget_Tracker.db;

public class TransactionsTable {

    public static final String NAME = "transactions";
    public static final String FLD_ID = "id";
    public static final String FLD_CAT_ID = "category_id";
    public static final String FLD_DATE = "date";
    public static final String FLD_DETAILS = "details";
    public static final String FLD_AMOUNT = "amount";

}
