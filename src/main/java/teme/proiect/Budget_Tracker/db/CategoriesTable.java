package teme.proiect.Budget_Tracker.db;

public class CategoriesTable {

    public static final String NAME = "categories";
    public static final String FLD_ID = "id";
    public static final String FLD_DESC = "description";
    public static final String FLD_TYPE = "type";

}
